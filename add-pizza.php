<?php 

    include('config/db_connect.php');

    $pizzaName = $email = $ingredients = '';

    $errors = ['email' => '', 'pizzaName'=> '', 'ingredients' => ''];

    if(isset($_POST['submit'])){

    //check email
        if(empty($_POST['email'])){
            $errors['email'] = 'An email is required <br/>';
        }else{ 

            $email = $_POST['email'];

            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $errors['email'] = 'Invalid email address entered.';
            }

            // echo htmlspecialchars($_POST['email']);
        }

     //check pizza name
        if(empty($_POST['pizzaName'])){
            $errors['pizzaName'] = 'A pizza name is required <br/>';
        }else{

            $pizzaName = $_POST['pizzaName'];

            if(!preg_match('/^[a-zA-Z\s]+$/', $pizzaName)){
                $errors['pizzaName'] = 'Name must be letter and spaces only';
            }
        }
        
    //check ingredients
        if(empty($_POST['ingredients'])){
            $errors['ingredients'] = 'At least one ingredients is required <br/>';
        }else{
            $ingredients = $_POST['ingredients'];

            if(!preg_match('/^([a-zA-Z\s]+)(,\s*[a-zA-Z\s]*)*$/', $ingredients)){
                $errors['ingredients'] = 'Ingredients must be a comma separated list <br/>';
            }
        }

        if(array_filter($errors)){
            echo 'there are errors in the form';
        }else{
            //save data to the database 

        $email = mysqli_real_escape_string($conn, $_POST['email']);
        $pizzaName = mysqli_real_escape_string($conn, $_POST['pizzaName']);
        $ingredients = mysqli_real_escape_string($conn, $_POST['ingredients']);

        //create sql
        $sql = "INSERT INTO  pizzas(pizzaName, email, ingredients) VALUES('$pizzaName', '$email', '$ingredients')";

        //save to DB and check
        if(mysqli_query($conn, $sql)){
            // success
            header('Location: index.php');
        } else {
            //error
            echo 'query error: ' . mysqli_error($conn); 
        }

        }
    
    } //end of POST check

?>

<!DOCTYPE html>
<html lang="en">

<?php include('templates/header.php');?>

<section class="container black-text">
    <h4 class="center">
        Add a Pizza
    </h4>

    <form class="white" action="<?php echo $_SERVER['PHP_SELF']?>" method="POST">
        <label>Your email</label>
        <input type="text" name="email" value="<?php echo htmlspecialchars($email) ?>">
        <div class="red-text">
            <?php echo $errors['email'] ?>
        </div>

        <label>Pizza Name</label>
        <input type="text" name="pizzaName" value="<?php echo htmlspecialchars($pizzaName) ?>">
        <div class="red-text">
            <?php echo $errors['pizzaName'] ?>
        </div>

        <label>Ingredients (comma separated):</label>
        <input type="text" name="ingredients" value="<?php echo htmlspecialchars($ingredients) ?>">
        <div class="red-text">
            <?php echo $errors['ingredients'] ?>
        </div>

        <div class="center">
            <input type="submit" name="submit" value="submit" class="btn brand z-depth-0">
        </div>
    </form>
</section>

<?php include('templates/footer.php');?>

    
</html>