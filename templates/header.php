<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Ninja Website</title>
     <!-- Compiled and minified CSS -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

     <style type="text/css">
        .brand{
            background-color: #8FD129 !important;
        }
        .brand-text{
            color: #8FD129 !important;
        }

        form{
            max-width: 600px;
            margin: 20px auto;
            padding: 20px;
        }

        .pizza{
            width: 100px;
            margin: 40px auto -60px;
            display:block;
            position: relative;
            top: -40px;
        }
        

       
    </style>
</head>
<body class='green lighten-4'>
    <nav class="white z-depth-0">
        <div class="container">
            <a href="index.php" class="brand-logo brand-text">Ninja Turtle Pizza</a>
            <ul id="nav-mobile" class="right hide-on-small-and-down">
                <li><a href="add-pizza.php" class="btn brand z-depth-0">Add a Pizza</a></li>

            </ul>
        </div> 
    </nav>
